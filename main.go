package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Customer struct {
	Cust_id  string `json:"cust_id"`
	Nama     string `json:"nama"`
	Alamat   string `json:"alamat"`
	Kode_pos string `json:"kode_pos"`
	No_hp    string `json:"no_hp"`
	Email    string `json:"email"`
}

var Customers []Customer

func returnAllCustomers(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(Customers)
}

func returnSingleCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["cust_id"]

	for _, customer := range Customers {
		if customer.Cust_id == key {
			json.NewEncoder(w).Encode(customer)
		}
	}
}

func createNewCustomer(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var customer Customer
	json.Unmarshal(reqBody, &customer)
	Customers = append(Customers, customer)
	json.NewEncoder(w).Encode(customer)
}

func updateCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["cust_id"]

	for index, customer := range Customers {
		if customer.Cust_id == id {
			Customers = append(Customers[:index], Customers[index+1:]...)
			var newCustomer Customer
			reqBody, _ := ioutil.ReadAll(r.Body)
			json.Unmarshal(reqBody, &newCustomer)
			newCustomer.Cust_id = id
			Customers = append(Customers, newCustomer)
			json.NewEncoder(w).Encode(newCustomer)

		}
	}
}

func deleteCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["cust_id"]

	for index, customer := range Customers {
		if customer.Cust_id == id {
			Customers = append(Customers[:index], Customers[index+1:]...)
		}
	}
}

func handleRequests() {
	myRoute := mux.NewRouter().StrictSlash(true)
	myRoute.HandleFunc("/", returnAllCustomers)
	myRoute.HandleFunc("/customer", createNewCustomer).Methods("POST")
	myRoute.HandleFunc("/customer/{cust_id}", updateCustomer).Methods("PUT")
	myRoute.HandleFunc("/customer/{cust_id}", deleteCustomer).Methods("DELETE")
	myRoute.HandleFunc("/customer/{cust_id}", returnSingleCustomer)

	log.Fatal(http.ListenAndServe(":5000", myRoute))
}

func main() {
	Customers = []Customer{
		Customer{Cust_id: "1", Nama: "John", Alamat: "Jerman", Kode_pos: "76235", No_hp: "085264532674", Email: "john@email.com"},
		Customer{Cust_id: "2", Nama: "Sahn", Alamat: "Amerika", Kode_pos: "13665", No_hp: "085264216674", Email: "sahn@email.com"},
	}
	handleRequests()
}
